<?php
/**
 * @file
 * Code for the sd_media feature.
 */

include_once 'sd_media.features.inc';

define('MEDIA_UPLOAD_FIELD_STEP', 4);

/**
 * Adds the tags field to the initial upload page on the media browser
 *
 * Also adds some JS to skip the 'fields' section of the media browser
 * worklfow as this is all taken care of on the first page
 */
function sd_media_form_file_entity_add_upload_alter(&$form, &$form_state) {
  // This isn't really necessary but ensures the usage consistency over all
  // upload forms.
  switch ($form['#step']) {
    // Add folder selection to the upload form.
    case 1:
      // By re-using the field form structure we can inject the value into the
      // next step.
      // @todo Check if we can replace this somehow by the real field widget.
      $form['field_media_tags'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
      );
      $form['field_media_tags'][LANGUAGE_NONE] = array(
        '#type' => 'textfield',
        '#title' => t('Tags'),
        '#description' => t('Select the tags that best describe this media. Each tag should be separated by a comma'),
        '#autocomplete_path' => 'taxonomy/autocomplete/field_media_tags',
      );

      break;

    case 2:
    break;
    case 3:
    break;
    case MEDIA_UPLOAD_FIELD_STEP:

      if (isset($form['field_folder'])) {
        $form['#attached']['js'] = array(drupal_get_path('module', 'sd_media') . '/js/sd_media.js');
      }

    break;
  }
}

/**
 * Alter the internet media add form to allow users to set the
 * folder and add tags when initially adding the media
 */
function sd_media_form_media_internet_add_alter(&$form, &$form_state) {

  $form['embed_code']['#weight'] = 0;

  // Add in the tags field
  $form['field_media_tags_video'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );
  $form['field_media_tags_video'][LANGUAGE_NONE] = array(
    '#type' => 'textfield',
    '#title' => t('Tags'),
    '#description' => t('Select the tags that best describe this media. Each tag should be separated by a comma'),
    '#autocomplete_path' => 'taxonomy/autocomplete/field_media_tags',
  );

  $form['field_media_tags_video']['#weight'] = 1;

  // Add in the folder field
  $form['field_folder_video'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );
  // Abuse the taxonomy term field widget to get the available options.
  $field['settings']['allowed_values'][] = array(
    'vocabulary' => 'media_folders',
    'parent' => 0,
  );
  $form['field_folder_video'][LANGUAGE_NONE] = array(
    '#type' => 'select',
    '#title' => t('Folder'),
    '#description' => t('Defines the folder where the uploaded files will be saved'),
    '#options' => taxonomy_allowed_values($field),
  );

  $form['field_folder_video']['#weight'] = 2;

  $form['providers']['#weight'] = 3;

  // Override the internet media submit handler with our own so we can
  // save the tags and folder on submit
  $form['#submit'][0] = 'sd_media_media_internet_add_submit';
}

/**
 * Upload a file from a URL.
 *
 * This will copy a file from a remote location and store it locally.
 *
 * @see media_parse_to_uri()
 * @see media_parse_to_file()
 */
function sd_media_media_internet_add_submit($form, &$form_state) {
  $embed_code = $form_state['values']['embed_code'];

  try {
    // Save the remote file
    $provider = media_internet_get_provider($embed_code);
    // Providers decide if they need to save locally or somewhere else.
    // This method returns a file object
    $file = $provider->save();
  }
  catch (Exception $e) {
    form_set_error('embed_code', $e->getMessage());
    return;
  }

  // Save the tags
  if (isset($form_state['values']['field_media_tags_video'][LANGUAGE_NONE]) && !empty($form_state['values']['field_media_tags_video'][LANGUAGE_NONE])) {

    $terms_string = $form_state['values']['field_media_tags_video'][LANGUAGE_NONE];
    $terms_list = explode(',', $terms_string);

    foreach ($terms_list as $term_name) {
      $name = trim($term_name);
      if ($term = taxonomy_get_term_by_name($name, 'media_tags')) {
        // add the term ID to the list
        $popped_term = array_pop($term);
        $file->field_media_tags[LANGUAGE_NONE][] = array('tid' => $popped_term->tid);
      }
    }
  }

  // Save the folder
  if (isset($form_state['values']['field_folder_video'][LANGUAGE_NONE]) && !empty($form_state['values']['field_folder_video'][LANGUAGE_NONE])) {
    $file->field_folder[LANGUAGE_NONE][0]['tid'] = $form_state['values']['field_folder_video'][LANGUAGE_NONE];
  }

  // Save the file with the updated fields
  file_save($file);

  if (!$file->fid) {
    form_set_error('embed_code', t('The file %file could not be saved. An unknown error has occurred.', array('%file' => $embed_code)));
    return;
  }
  else {
    $form_state['file'] = $file;
  }

  // Redirect to the file edit page after submission.
  if (file_entity_access('update', $file)) {
    $destination = array('destination' => 'admin/content/file');
    if (isset($_GET['destination'])) {
      $destination = drupal_get_destination();
      unset($_GET['destination']);
    }
    $form_state['redirect'] = array('file/' . $file->fid . '/edit', array('query' => $destination));
  }
  else {
    $form_state['redirect'] = 'admin/content/file';
  }
}

/**
 * Hides the alt and title fields when a user edits an uploaded file
 */
/*function sd_media_form_file_entity_edit_alter(&$form, &$form_state) {

  // Attach our own submit handler to flush the image caches after a crop has been selected
  $form['actions']['submit']['#submit'][] = 'sd_media_clear_image_caches';
}*/

/**
 * Custom submit handler for the file entity alter form
 */
/*function sd_media_clear_image_caches($form, &$form_state) {

  $fid = $form_state['values']['fid'];

  foreach ($form_state['values']['manualcrop']['file_' . $fid]['manualcrop_selections'] as $style => $coordinates) {
    if (!empty($coordinates)) {
      $styles[] = image_style_load($style);
    }

  }

  if (!empty($styles)) {
    // Clear the caches for each image style
    _sd_media_image_style_flush_lite($styles);
  }
}*/

/**
 * Implements hook_field_formatter_info().
 *
 * Declares the 'Any media' field formatter
 */
function sd_media_field_formatter_info() {
  $info['any_media'] = array(
    'label' => t('Any Media'),
    'description' => t('Display the video in a specific view mode'),
    'field types' => array('file'),
    'settings' => array(
      'styles' => NULL,
      'image_link' => NULL,
      'image_title' => NULL,
      'image_alt' => NULL,
      'image_caption' => NULL,
      'width' => '640',
      'height' => '390',
      'video_cover' => NULL,
      'use_colorbox' => FALSE,
    ),
    'file formatter' => array(
      'hidden' => TRUE,
    ),
  );

  return $info;
}

/**
 * Implements hook_field_formatter_settings_form().
 *
 * Sets up the settings form for the 'Any media' field formatter
 */
function sd_media_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = array();

  // Add the form elements for the any media field
  if ($display['type'] == 'any_media') {

    $image_styles = image_style_options(FALSE, PASS_THROUGH);

    $element['styles'] = array(
      '#tree' => TRUE,
      '#theme' => 'sd_media_image_styles_form',
    );
    for ($i = 1; $i <= 3; $i++) {
      $element['styles']['breakpoint_' . $i] = array(
        '#title' => t('Client width breakpoint @key', array('@key' => $i)),
        '#title_display' => 'invisible',
        '#type' => 'textfield',
        '#default_value' => isset($settings['styles']['breakpoint_' . $i]) ? $settings['styles']['breakpoint_' . $i] : '',
        '#maxlength' => 5,
        '#size' => 5,
        '#field_suffix' => 'px',
        '#element_validate' => array('element_validate_integer_positive'),
      );
      $element['styles']['style_' . $i] = array(
        '#title' => t('Image style for breakpoint @key', array('@key' => $i)),
        '#title_display' => 'invisible',
        '#type' => 'select',
        '#default_value' => isset($settings['styles']['style_' . $i]) ? $settings['styles']['style_' . $i] : '',
        '#empty_option' => t('None (original image)'),
        '#options' => $image_styles,
      );
    }
    $element['styles']['max_style'] = array(
      '#title' => t('Maximum'),
      '#title_display' => 'invisible',
      '#description' => t('Image style to use when the client width exceeds the widest value specified above.'),
      '#type' => 'select',
      '#default_value' => $settings['styles']['max_style'],
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    );
    $element['styles']['fallback_style'] = array(
      '#title' => t('Fallback'),
      '#title_display' => 'invisible',
      '#description' => t('Image style to use when the client does not support JavaScript.'),
      '#type' => 'select',
      '#default_value' => $settings['styles']['fallback_style'],
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    );

    $link_types = sd_media_image_field_link_types($instance);

    $element['image_link'] = array(
      '#title' => t('Link image to'),
      '#type' => 'select',
      '#default_value' => $settings['image_link'],
      '#empty_option' => t('Nothing'),
      '#options' => $link_types,
    );

    $element['image_title'] = array(
      '#type' => 'select',
      '#title' => t('Image title text'),
      '#default_value' => $settings['image_title'],
      '#empty_option' => t('No title text'),
      '#options' => $link_types,
    );

    $element['image_alt'] = array(
      '#type' => 'select',
      '#title' => t('Image alt text'),
      '#default_value' => $settings['image_alt'],
      '#empty_option' => t('No alt text'),
      '#options' => $link_types,
    );

    $element['image_caption'] = array(
      '#type' => 'select',
      '#title' => t('Image caption'),
      '#default_value' => $settings['image_caption'],
      '#empty_option' => t('No caption'),
      '#options' => $link_types,
    );

    $element['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Video width'),
      '#default_value' => $settings['width'],
      '#size' => 5,
      '#maxlength' => 5,
      '#field_suffix' => t('pixels'),
    );

    $element['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Video height'),
      '#default_value' => $settings['height'],
      '#size' => 5,
      '#maxlength' => 5,
      '#field_suffix' => t('pixels'),
    );

    $element['video_cover'] = array(
      '#title' => t('Video cover'),
      '#type' => 'select',
      '#default_value' => $settings['video_cover'],
      '#empty_option' => t('Nothing'),
      '#options' => $link_types,
    );

    $element['use_colorbox'] = array(
      '#type' => 'checkbox',
      '#title' => t('Open video in overlay'),
      '#default_value' => $settings['use_colorbox'],
      '#description' => t('Display video in an overlay'),
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 *
 * Displays the settings form for the 'Any media' field formatter on the field collection
 * manage display page
 */
function sd_media_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = array();

  for ($i = 1; $i <= 3; $i++) {
    if (isset($settings['styles']['breakpoint_' . $i]) && isset($settings['styles']['style_' . $i])) {
      $breakpoint = $settings['styles']['breakpoint_' . $i];
      if ($breakpoint > 0) {
        $summary[] = t('Client widths up to @width px &rarr; Image style: @style', array('@width' => $breakpoint, '@style' => _sd_media_image_view_image_style($settings['styles']['style_' . $i])));
      }
    }
  }
  $summary[] = t('Maximum &rarr; Image style: @style', array('@style' => _sd_media_image_view_image_style(isset($settings['styles']['max_style']) ? $settings['styles']['max_style'] : '')));
  $summary[] = t('Fallback &rarr; Image style: @style', array('@style' => _sd_media_image_view_image_style(isset($settings['styles']['fallback_style']) ? $settings['styles']['fallback_style'] : '')));

  $link_types = sd_media_image_field_link_types($instance);
  // Display this setting only if image is linked.
  if (isset($link_types[$settings['image_link']])) {
    $link = filter_xss_admin($link_types[$settings['image_link']]);
    $summary[] = t('Image link provided by <em>@field</em>.', array('@field' => $settings['image_link']));
  }

  // Display the image properties
  if (isset($settings['image_title']) && !empty($settings['image_title'])) {
    $summary[] = t('Image title provided by <em>@field</em> field.', array('@field' => $settings['image_title']));
  }
  else {
    $summary[] = t('No image title.');
  }

  if (isset($settings['image_alt']) && !empty($settings['image_alt'])) {
    $summary[] = t('Image alt text provided by <em>@field</em> field.', array('@field' => $settings['image_alt']));
  }
  else {
    $summary[] = t('No image alt text.');
  }

  if (isset($settings['image_caption']) && !empty($settings['image_caption'])) {
    $summary[] = t('Image caption provided by <em>@field</em> field.', array('@field' => $settings['image_caption']));
  }
  else {
    $summary[] = t('No image caption.');
  }

  // Set the default values here. As we don't know if this is vimeo or
  // Youtube we will just provide the generic default size
  if (empty($settings['width'])) {
    $settings['width'] = 640;
  }

  if (empty($settings['height'])) {
    $settings['height'] = 390;
  }

  if ($display['type'] == 'any_media') {
    $summary[] = t('Video player size: %width x %heightpx', array('%width' => $settings['width'], '%height' => $settings['height']));
  }


  if (isset($settings['video_cover']) && !empty($settings['video_cover'])) {
    $summary[] = t('Video cover provided by <em>@field</em> field.', array('@field' => $settings['video_cover']));
  }
  else {
    $summary[] = t('No video cover.');
  }

  $use_colorbox = isset($settings['use_colorbox']) && !empty($settings['use_colorbox']) ? 'Yes' : 'No';
  $summary[] = t('Video in colobox: <em>@value</em>.', array('@value' => $use_colorbox));

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 *
 * Displays the file depending on the type
 */
function sd_media_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $settings = $display['settings'];
  $element = array();

  if ($display['type'] == 'any_media') {

    // @todo - This should loop over items instead of just trying to grab the first one

    if (!empty($items[0]['filemime'])) {

      $mimetype_parts = explode('/', $items[0]['filemime']);

      $file_type = $mimetype_parts[0]; // image or video
      $file_format = $mimetype_parts[1]; // youtube or vimeo

      switch ($file_type) {
        case 'image':

          _sd_media_get_image_element($entity_type, $entity, $field, $instance, $langcode, $items, $display, $settings, $element);

        break;
        case 'video':

          // Theme the youtube player
          if ($file_format == 'youtube') {
            // Youtube doesn't expose the defaults, so this is copied from the module
            $options = array(
              'width' => 640,
              'height' => 390,
              'autohide' => 2,
              'autoplay' => FALSE,
              'color' => 'red',
              'enablejsapi' => FALSE,
              'loop' => FALSE,
              'modestbranding' => FALSE,
              'nocookie' => FALSE,
              'origin' => '',
              'protocol' => 'https:',
              'protocol_specify' => FALSE,
              'rel' => TRUE,
              'showinfo' => TRUE,
              'theme' => 'dark',
            );

            $theme = 'media_youtube_video';
          }
          // Grab the vimeo options and output the player
          elseif ($file_format == 'vimeo') {
            // no longer supported
            // $options = media_vimeo_variable_default();
            $options = array(
              'width' => 640,
              'height' => 390,
              'autoplay' => FALSE,
            );
            $theme = 'media_vimeo_video';
          }

          if (!empty($display['settings']['width'])) {
            $options['width'] = $display['settings']['width'];
          }

          if (!empty($options['height'])) {
            $options['height'] = $display['settings']['height'];
          }

          $options['use_colorbox'] = isset($display['settings']['use_colorbox']) && !empty($display['settings']['use_colorbox']);

          $element = array(
            '#theme' => $theme,
            '#uri' => $items[0]['uri'],
            '#options' => $options,
          );

          // dpm($element);

        break;
        case 'application':

          // Hard coded this, needs updating
          $view_mode = 'default';

          // To prevent infinite recursion caused by reference cycles, we store
          // diplayed nodes in a recursion queue.
          $recursion_queue = &drupal_static(__FUNCTION__, array());

          // If no 'referencing entity' is set, we are starting a new 'reference
          // thread' and need to reset the queue.
          // @todo Bug: $entity->referencing_entity on files referenced in a different
          // thread on the page.
          // E.g: 1 references 1+2 / 2 references 1+2 / visit homepage.
          // We'd need a more accurate way...
          if (!isset($entity->referencing_entity)) {
            $recursion_queue = array();
          }

          // The recursion queue only needs to track files.
          if ($entity_type == 'file') {
            list($id) = entity_extract_ids($entity_type, $entity);
            $recursion_queue[$id] = $id;
          }

          // Prevent 'empty' fields from causing a WSOD.
          $items = array_filter($items);

          // Check the recursion queue to determine which nodes should be fully
          // displayed, and which nodes will only be displayed as a title.
          $files_display = array();
          foreach ($items as $delta => $item) {
            if (!isset($recursion_queue[$item['fid']])) {
              $files_display[$item['fid']] = file_load($item['fid']);
            }
          }

          // Load and build the fully displayed nodes.
          if ($files_display) {
            foreach ($files_display as $fid => $file) {
              $files_display[$fid]->referencing_entity = $entity;
              $files_display[$fid]->referencing_field = $field['field_name'];
            }
            $output = file_view_multiple($files_display, $view_mode);
            // Remove the first level from the output array.
            $files_built = reset($output);
          }

          // Assemble the render array.
          foreach ($items as $delta => $item) {
            if (isset($files_built[$item['fid']])) {
              $element[$delta] = $files_built[$item['fid']];
            }
          }

        break;
        }
      }
    }

  return $element;
}

/**
 * Helper function to compute the list of possible link types.
 *
 * Copy of image_link_formatter_image_field_link_types() from the
 * image link formatter module
 */
function sd_media_image_field_link_types($instance) {
  $link_types = array(
    'content' => t('Content'),
    'file' => t('File'),
  );
  // If the link module is installed, also allow any link fields to be used.
  $allowed_field_types = array('link_field', 'text', 'text_long', 'file');
  foreach (field_info_fields() as $field_key => $field_info) {
    if (in_array($field_info['type'], $allowed_field_types)) {
      $field_instance = field_info_instance($instance['entity_type'], $field_info['field_name'], $instance['bundle']);
      if ($field_instance) {
        $link_types[$field_key] = "$field_instance[label] ($field_info[field_name])";
      }
    }
  }
  return $link_types;
}

/**
 * Returns HTML for an image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: Associative array of image data, which may include "uri", "alt",
 *     "width", "height", "title" and "attributes".
 *   - image_style: An optional image style.
 *   - path: An array containing the link 'path' and link 'options'.
 *
 * @ingroup themeable
 */
function theme_sd_media_formatter($variables) {

  $item = $variables['item'];

  $image = array(
    'path' => $item['uri'],
  );

  if (array_key_exists('alt', $item)) {
    $image['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }

  $image['attributes']['class'][] = 'any-media-image';

  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  // Title attribute, if not empty.
  if (drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }

  // Width and height attributes.
  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  // Output fallback image that will work without JavaScript.
  $image_output = _sd_media_image_view_image($image, $variables['fallback_style']);

  // Prepare attributes that will be picked up by our JavaScript code
  // to serve an adapted image. Some browsers cannot access the
  // children of a <noscript> element, thus all the data needs to be
  // attached to the <noscript> element.
  $image['attributes']['class'][] = 'adaptive-image';
  $attributes['class'][] = 'adaptive-image';
  if (isset($variables['breakpoint_styles'])) {
    foreach ($variables['breakpoint_styles'] as $breakpoint => $style_name) {
      $image['attributes']['data-adaptive-image-breakpoint'] = $breakpoint;
      $attributes['data-adaptive-image-breakpoints'][] = $breakpoint; // Append to list of breakpoints.
      $attributes['data-adaptive-image-' . $breakpoint . '-img'] = _sd_media_image_view_image($image, $style_name);
    }
  }
  $image['attributes']['data-adaptive-image-breakpoint'] = 'max';
  $attributes['data-adaptive-image-max-img'] = _sd_media_image_view_image($image, $variables['max_style']);

  // Output the <noscript> element with its data attributes.
  $output = theme('html_tag', array('element' => array('#tag' => 'noscript', '#value' => $image_output, '#attributes' => $attributes)));

  // Output the <noscript> element with its data attributes.
  if (isset($variables['path']['path']) && drupal_strlen($variables['path']['path']) > 0) {
    $options = isset($variables['path']['options']) ? $variables['path']['options'] : array();
    $image_text = '';
    if (isset($options['attributes']['title'])  && $variables['fallback_style']!=='news_thumbnail') {
      $image_text .= '<span class="text-table">';
      $image_text .= '<span class="text-row">';
      $image_text .= '<span class="text-cell">';
      $image_text .= $options['attributes']['title'];
      $image_text .= '</span></span></span>';
    }
    $output = l($output . $image_text, $variables['path']['path'], array_merge($variables['path']['options'], array('html' => TRUE)));
  }

  // Add a wrapper for theming
  $output = '<div class="any-media-wrapper ' . $variables['fc_view_mode'] . '">' . $output;

  // Add the caption in its own wrapper
  if (!empty($variables['image_caption'])) {
    $output .= '<div class="caption-wrapper">' . $variables['image_caption'] . '</div>';
  }

  $output .= '</div>';

  return $output;
}

/**
 * Outputs an image based on the specified image style.
 */
function _sd_media_image_view_image($image, $style_name) {
  if ($style_name) {
    $image['style_name'] = $style_name;
    return theme('image_style', $image);
  }
  else {
    return theme('image', $image); // Original image.
  }
}

/**
 * Hook in js files to override the default
 * media browser behaviour
 */
function sd_media_library_alter(&$libraries, $module) {

	$path = drupal_get_path('module', 'sd_media');

	// Add our overrides to media.browser.js - sets the browser height
	if (isset($libraries['media_browser_page'])) {
		$libraries['media_browser_page']['js'][$path . '/js/sd.media.browser.js']  = array('group' => JS_DEFAULT);
	}

	// Add our overrides to media.popups.js - sets the browser width
	if (isset($libraries['media_browser'])) {
		$libraries['media_browser']['js'][$path . '/js/sd.media.popups.js']  = array('group' => JS_DEFAULT);
	}
}

/**
 * Displays the name of an image style, ensuring that the style actually exists.
 */
function _sd_media_image_view_image_style($style_name) {
  if ($style_name) {
    $image_styles = image_style_options(FALSE);
    // Unset the potential 'No defined styles' option.
    unset($image_styles['']);

    // Ensure that we return an existing style.
    if (isset($image_styles[$style_name])) {
      return $image_styles[$style_name];
    }
  }
  return t('Original image'); // Default.
}

/**
 * Preprocess function for theme_cs_adaptive_image_formatter().
 */
function sd_media_preprocess_sd_media_formatter(&$variables) {
  drupal_add_js(drupal_get_path('module', 'sd_media') . '/js/sd_media.adaptive.js', 'file');
}

/**
 * Checks if the specified style exists. If it does not exists, return
 * the original image style.
 */
function _sd_media_image_check_image_style($style_name) {
  $image_styles = image_style_options();
  if (isset($image_styles[$style_name])) {
    return $style_name;
  }
  else {
    return '';
  }
}

/**
 * Flushes cached media for a style. It DOESNT then
 * flush the page cache and rebuild the registry which is expensive
 *
 * @param $style
 *   An image style array.
 */
/*function _sd_media_image_style_flush_lite($styles) {
  // Delete the style directory in each registered wrapper.
  $wrappers = file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE);

  // Clear the styles caches delete already rendered images
  foreach ($styles as $style) {
    foreach ($wrappers as $wrapper => $wrapper_data) {
      if (file_exists($directory = $wrapper . '://styles/' . $style['name'])) {
        file_unmanaged_delete_recursive($directory);
      }
    }

    // Let other modules update as necessary on flush.
    module_invoke_all('image_style_flush', $style);
  }

  // Clear image style and effect caches.
  cache_clear_all('image_styles', 'cache');
  cache_clear_all('image_effects:', 'cache', TRUE);
  drupal_static_reset('image_styles');
  drupal_static_reset('image_effects');
}*/

/**
 * Implements hook_theme().
 */
function sd_media_theme() {
  return array(
    // Theme functions in image.module.
    'sd_media_formatter' => array(
      'variables' => array(
        'item' => NULL,
        'path' => NULL,
        'image_caption' => NULL,
        'fc_view_mode' => NULL,
        'breakpoint_styles' => NULL,
        'max_style' => NULL,
        'fallback_style' => NULL
      ),
    ),
    'sd_media_image_formatter' => array(
      'variables' => array('item' => NULL, 'path' => NULL, 'breakpoint_styles' => NULL, 'max_style' => NULL, 'fallback_style' => NULL),
    ),
    'sd_media_image_styles_form' => array(
      'render element' => 'styles',
      'file' => 'sd_media.admin.inc',
    ),
  );
}

 function sd_media_permission() {
  return array(
    'sd administer media browser plus' => array(
      'title' => t('Administer media browser plus. (SD version)'),
      'description' => t('Access media browser plus settings.'),
    )
  );
}

function sd_media_menu_alter(&$items) {
  $items['admin/config/media/media_browser_plus_settings']['access arguments'] = array('sd administer media browser plus');
}

function _sd_media_get_image_element(&$entity_type, &$entity, &$field, &$instance, &$langcode, &$items, &$display, &$settings, &$element){
  // Check if the formatter involves a link.
  $image_link = $display['settings']['image_link'];

  // Check if the formatter involves a link.
  if ($display['settings']['image_link'] == 'content') {

    // If this field is in a field collection then we
    // need to go up the chain to get the node
    if ($entity_type == 'field_collection_item') {

      // Grab the node entity
      $host_entity = $entity->hostEntity();

      // Set the link to the host entity
      if (!empty($host_entity->nid)) {
        $uri = 'node/' . $host_entity->nid;
      }
    }
    else {
      $uri = entity_uri($entity_type, $entity);
    }
  }
  elseif ($display['settings']['image_link'] == 'file') {
    $link_file = TRUE;
  }
  elseif ($image_link) {
    // This is a link to a field
    if (isset($entity->$image_link)) {
      // Support for field translations.
      $language = field_language($entity_type, $entity, $field['field_name']);
      $link_field = $entity->$image_link;

      if (isset($link_field[$language])) {
        $link_values = $link_field[$language];
      }
    }
  }

  // Check if we have a title and alt to add to this image from the node
  $image_title = '';
  if (isset($display['settings']['image_title']) && !empty($display['settings']['image_title']) && !empty($entity->{$display['settings']['image_title']}[LANGUAGE_NONE][0]['value'])) {
    $image_title = $entity->{$display['settings']['image_title']}[LANGUAGE_NONE][0]['value'];
  }

  $image_alt = '';
  if (isset($display['settings']['image_alt']) && !empty($display['settings']['image_alt']) && !empty($entity->{$display['settings']['image_alt']}[LANGUAGE_NONE][0]['value'])) {
    $image_alt = $entity->{$display['settings']['image_alt']}[LANGUAGE_NONE][0]['value'];
  }

  $image_caption = '';
  if (isset($display['settings']['image_caption']) && !empty($display['settings']['image_caption']) && !empty($entity->{$display['settings']['image_caption']}[LANGUAGE_NONE][0]['value'])) {
    $image_caption = $entity->{$display['settings']['image_caption']}[LANGUAGE_NONE][0]['value'];
  }

  // Create the image
  foreach ($items as $delta => $item) {

    if (isset($link_file)) {
      $uri = file_create_url($item['uri']);
    }

    // Handle multiple link with image values.
    if (isset($link_values)) {
      if (isset($link_values[$delta]['url'])) {
        $uri = $link_values[$delta]['url'];
      }
      // If there are more image values than link values unset the link.
      else {
        unset($uri);
      }
    }

    // Add the title, alt if they are present
    // if link we want to move the title to the anchor
    if ($image_title && !isset($uri)) {
      $item['title'] = $image_title;
    }

    if ($image_alt) {
      $item['alt'] = $image_alt;
    }

    // If link set title attribute
    if (isset($uri)) {
      $link_attr = array('title' => $image_title);
    }

    // Sort out the breakpoint styles here
    $styles = array();
    for ($i = 1; $i <= 3; $i++) {
      // If breakpoint is defined.
      if (isset($settings['styles']['breakpoint_' . $i]) && isset($settings['styles']['style_' . $i]) && $settings['styles']['breakpoint_' . $i] > 0) {
        // Associate valid style to breakpoint.
        $styles[$settings['styles']['breakpoint_' . $i]] = _sd_media_image_check_image_style($settings['styles']['style_' . $i]);
      }
    }

    $element[$delta] = array(
      '#theme' => 'sd_media_formatter',
      '#item' => $item,
      '#path' => array(
        'path' => isset($uri) ? $uri : '',
        'options' => array(
          'attributes' => isset($link_attr) ? $link_attr : array()
        ),
      ),
      '#image_caption' => $image_caption,
      '#fc_view_mode' => 'full',
      '#breakpoint_styles' => $styles,
      '#max_style' => isset($settings['styles']['max_style']) ? _sd_media_image_check_image_style($settings['styles']['max_style']) : '',
      '#fallback_style' => isset($settings['styles']['fallback_style']) ? _sd_media_image_check_image_style($settings['styles']['fallback_style']) : '',
    );
  }
}
