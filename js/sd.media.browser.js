/**
 * @file
 * Provides default functions for the media browser
 */

(function ($) {
namespace('Drupal.media.browser');

Drupal.media.browser.selectedMedia = [];
Drupal.media.browser.activeTab = 0;
Drupal.media.browser.mediaAdded = function () {};
Drupal.media.browser.selectionFinalized = function (selectedMedia) {
  // This is intended to be overridden if a callee wants to be triggered
  // when the media selection is finalized from inside the browser.
  // This is used for the file upload form for instance.
};

/**
 * Resize the Media Browser to the content height.
 */
Drupal.media.browser.resizeIframe = function (event) {
  //var h = $('body').height();
  $(parent.window.document).find('#mediaBrowser').height(560);
};

}(jQuery));
