(function($) {

/**
 * Quick submit behaviour for the image upload
 *
 * Clicks the submit on the 'fields' section of the image upload
 * media process. The user doesn't need to fill in those fields so
 * push them onto the next bit.
 */
Drupal.behaviors.mediaQuickSubmit = {
  attach: function (context, settings) {
  	$('#edit-submit').click();

  	// Probably add a styled loading gif
  	var loading_html = '<div id="loading_text"><br />Your file is being saved. Please wait.<br /><br /></div>';

  	$('#media-tab-upload').html(loading_html);
  }
};
})(jQuery);
