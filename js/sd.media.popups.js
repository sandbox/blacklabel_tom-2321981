
/**
 * @file: Popup dialog interfaces for the media project.
 *
 * Drupal.media.popups.mediaBrowser
 *   Launches the media browser which allows users to pick a piece of media.
 *
 * Drupal.media.popups.mediaStyleSelector
 *  Launches the style selection form where the user can choose
 *  what format / style they want their media in.
 *
 */

(function ($) {
namespace('Drupal.media.popups');

/**
 * Generic functions to both the media-browser and style selector
 */

/**
 * Returns the commonly used options for the dialog.
 */
Drupal.media.popups.getDialogOptions = function () {
  return {
    buttons: {},
    dialogClass: 'media-wrapper',
    modal: true,
    draggable: false,
    resizable: true,
    minWidth: '100%',
    width: '100%',
    height: 560,
    position: 'center',
    overlay: {
      backgroundColor: '#000000',
      opacity: 0.4
    },
    zIndex: 10000,
    close: function (event, ui) {
      $(event.target).remove();
    },
    open: function (event, ui) {
      // Re-size the ui dialog when this opens
      // to give a small gutter
      ui_dialog = $(event.target).parent();
      ui_dialog.css({
        width: '80%',
        left: '10%'
      });
    }
  };
};

var css = document.createElement("style");
css.type = "text/css";
css.innerHTML = "#modalContent .modal-header { background: #e0e0d8; color: black; padding: 10px; }";
css.innerHTML += "#modalContent .modal-title  { color: black; }";
css.innerHTML += "#modalContent .modal-content  { padding: 0 .9em; }";
css.innerHTML += "#modalContent .close  { color: black; }";
css.innerHTML += "#modalContent .close img  { display:none; }";
css.innerHTML += "#modalBackdrop {background-color: #000 !important; opacity: .70 !important; }";
$('head').append(css);

})(jQuery);
