<?php

/**
 * @file media_vimeo/themes/media-vimeo-video.tpl.php
 *
 * Template file for theme('media_vimeo_video').
 *
 * Variables available:
 *  $uri - The media uri for the Vimeo video (e.g., vimeo://v/xsy7x8c9).
 *  $video_id - The unique identifier of the Vimeo video (e.g., xsy7x8c9).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Vimeo iframe.
 *  $options - An array containing the Media Vimeo formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: Vimeo file display options.
 *  $height - The height value set in Media: Vimeo file display options.
 *  $title - The Media: YouTube file's title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 *
 */

?>
<div class="<?php print $classes; ?> media-vimeo-<?php print $id; ?>">
<?php if ($options['use_colorbox']): ?>


  <button class="btn" data-toggle="modal" data-target="#video-modal-<?php print $video_id; ?>"><span>Play</span></button>

  <div class="modal fade" id="video-modal-<?php print $video_id; ?>" tabindex="-1" role="dialog" aria-labelledby="Location">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title"><?php print $title ?></h4>
        </div>
        <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="media-vimeo-player embed-responsive-item" <?php print $api_id_attribute; ?>width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="0" allowfullscreen><?php print $alternative_content; ?></iframe>
          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<?php else: ?>
  <div class="embed-responsive embed-responsive-16by9">
    <iframe class="media-vimeo-player embed-responsive-item" <?php print $api_id_attribute; ?>width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="0" allowfullscreen><?php print $alternative_content; ?></iframe>
  </div>
<?php endif ?>
</div>
