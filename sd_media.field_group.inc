<?php
/**
 * @file
 * sd_media.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sd_media_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_horizontal_tab_group|field_collection_item|field_content_slice|form';
  $field_group->group_name = 'group_horizontal_tab_group';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_content_slice';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Horizontal tab group',
    'weight' => '1',
    'children' => array(
      0 => 'group_text',
      1 => 'group_media',
      2 => 'group_quote',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'label' => 'Horizontal tab group',
      'instance_settings' => array(
        'classes' => 'group-horizontal-tab-group field-group-htabs',
      ),
    ),
  );
  $export['group_horizontal_tab_group|field_collection_item|field_content_slice|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_attributes|field_collection_item|field_media|form';
  $field_group->group_name = 'group_image_attributes';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_media';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Image attributes',
    'weight' => '1',
    'children' => array(
      0 => 'field_media_alt',
      1 => 'field_media_link',
      2 => 'field_media_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Image attributes',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-image-attributes field-group-fieldset',
        'description' => 'The following fields apply to <strong>images</strong> only',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_image_attributes|field_collection_item|field_media|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_media_attributes|field_collection_item|field_media|form';
  $field_group->group_name = 'group_media_attributes';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_media';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Media attributes',
    'weight' => '4',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Media attributes',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-media-attributes field-group-fieldset',
        'description' => 'The following fields apply to <strong>images</strong> only, and may render differently depending on the context of the media.',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_media_attributes|field_collection_item|field_media|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_media|field_collection_item|field_content_slice|form';
  $field_group->group_name = 'group_media';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_content_slice';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_horizontal_tab_group';
  $field_group->data = array(
    'label' => 'Media',
    'weight' => '9',
    'children' => array(
      0 => 'field_media',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-media field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_media|field_collection_item|field_content_slice|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_video_attributes|field_collection_item|field_media|form';
  $field_group->group_name = 'group_video_attributes';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_media';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Video attributes',
    'weight' => '2',
    'children' => array(
      0 => 'field_media_cover',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Video attributes',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-video-attributes field-group-fieldset',
        'description' => 'The following fields apply to <strong>videos</strong> only',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_video_attributes|field_collection_item|field_media|form'] = $field_group;

  return $export;
}
