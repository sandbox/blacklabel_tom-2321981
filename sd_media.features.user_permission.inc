<?php
/**
 * @file
 * sd_media.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function sd_media_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'add media from remote sources'.
  $permissions['add media from remote sources'] = array(
    'name' => 'add media from remote sources',
    'roles' => array(
      'admin' => 'admin',
    ),
    'module' => 'media_internet',
  );

  // Exported permission: 'administer media browser'.
  $permissions['administer media browser'] = array(
    'name' => 'administer media browser',
    'roles' => array(
      'admin' => 'admin',
    ),
    'module' => 'media',
  );

  // Exported permission: 'administer media wysiwyg view mode'.
  $permissions['administer media wysiwyg view mode'] = array(
    'name' => 'administer media wysiwyg view mode',
    'roles' => array(
      'admin' => 'admin',
    ),
    'module' => 'media_wysiwyg_view_mode',
  );

  // Exported permission: 'administer meta tags'.
  $permissions['administer meta tags'] = array(
    'name' => 'administer meta tags',
    'roles' => array(),
    'module' => 'metatag',
  );

  // Exported permission: 'delete terms in media_folders'.
  $permissions['delete terms in media_folders'] = array(
    'name' => 'delete terms in media_folders',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in media_tags'.
  $permissions['delete terms in media_tags'] = array(
    'name' => 'delete terms in media_tags',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit meta tags'.
  $permissions['edit meta tags'] = array(
    'name' => 'edit meta tags',
    'roles' => array(
      'admin' => 'admin',
    ),
    'module' => 'metatag',
  );

  // Exported permission: 'edit terms in media_folders'.
  $permissions['edit terms in media_folders'] = array(
    'name' => 'edit terms in media_folders',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in media_tags'.
  $permissions['edit terms in media_tags'] = array(
    'name' => 'edit terms in media_tags',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'import media'.
  $permissions['import media'] = array(
    'name' => 'import media',
    'roles' => array(
      'admin' => 'admin',
    ),
    'module' => 'media',
  );

  // Exported permission: 'sd administer media browser plus'.
  $permissions['sd administer media browser plus'] = array(
    'name' => 'sd administer media browser plus',
    'roles' => array(
      'admin' => 'admin',
    ),
    'module' => 'sd_media',
  );

  return $permissions;
}
